while True:
    import random
    import time

    print("       Ingresar a:")
    print(" 1.Metodos de ordenamiento")
    print(" 2.Metodos busqueda")
    print(" 3.Rendirse (salir)")


    opc = int(input("Ingresa opcion: "))

    if opc == 1:
        print(" A que tipo de ordenamiento quieres ingresar?")
        print(" 1. Radix")
        print(" 2. Shell")
        print(" 3. Burbuja")
        print(" 4. Baraja")
        print(" 5. Binaria")
        print(" 6. Quicksort")
        print(" 7. Mergesort")

        opc2 = int(input("Ingresa opcion: "))
        if opc2 == 1:
            print("\t ------------------METODO RADIX------------------")
            
            def radixsort(lista):
                n = 0
                for i in lista:
                     if len(i) > n:
                         n = len(i)
                
                for j in range(0, len(lista)):
                    while len(lista[j]) < n:
                        lista[j] = "0" + lista[j]

                for k in range(n - 1, -1, -1):
                    grupos = [[] for i in range(10)]

                    for i in range(len(lista)):
                        grupos[int(lista[i][k])].append(lista[i])
                
                    lista = []
                    for l in grupos:
                        lista +=l
                        print(grupos)
                return [int(i) for i in lista]
            lista = [str(random.randint(0, 100)) for i in range(20)]


            inicio=time.time()
            print("LISTA ALEATORIA:")
            print(f"{lista}\n-------------------------------------------------------------------------------")     
            print(f"\nLISTA ORDENADA \n{radixsort(lista)}")
            final=time.time()
            tiempo= (final-inicio)
            print(f"Tiempo total de ordenamiento: {tiempo}")
            
        elif opc2 == 2: 
            print("\t --------------------------Shell--------------------------")
            def shell(array, lista):
                inte = lista // 2
                while inte > 0:
                    for i in range(lista):
                        print(array)
                        t = array[i]
                        j = i
                        while j >= inte and array[j - inte] > t:
                            array[j] = array[j - inte]
                            j -= inte
                        array[j] = t
                    inte //= 2


            lista = [int(random.randint(1,100)) for i in range (20)]
            inicio=time.time()
            print("LISTA ALEATORIA")
            print(f"{lista}\n-------------------------------------------------------------------------------")
            size = len(lista)
            shell(lista, size)
            print("\n LISTA ORDENADA:")
            print(lista)
            final=time.time()
            tiempo= (final-inicio)
            print(f"Tiempo total de ordenamiento: {tiempo}")

        elif opc2 == 3:
            print("\t--------------------------Burbuja--------------------------")
            def Burbuja(array):                                                                             
                length=len(array)-1
                for i in range(0,length):
                    print(array)
                    for j in range(0, length):
                        if array[j] > array [j+1]:
                            aux=array[j]
                            array[j]=array[j+1]
                            array[j+1]=aux
                return array
            lista = [int(random.randint(0,100)) for i in range (20)]
            inicio =time.time()
            print("\n LISTA ORDENADA:")
            print(f"{lista}\n-------------------------------------------------------------------------------")
            print(Burbuja(lista))
            final =time.time()
            print(f"Tiempo total de ordenamiento: {tiempo}")
        
        elif opc2 == 4:
            print("\t--------------------------Baraja--------------------------")

            def baraja(array):
                for i in range(len(array)):
                    print(array)
                    for j in range(i, 0, -1):
                        if(array[j-1] > array[j]):
                            arr=array[j]
                            array[j]=array[j-1]
                            array[j-1]=arr
                print (array)
 
            lista=[int(random.randint(0,100)) for i in range (20)]
            inicio=time.time()
            print("LISTA ALEATORIA:")
            print(f"{lista}\n-------------------------------------------------------------------------------")
            baraja(lista)
            final=time.time()
            tiempo=(final-inicio)
            print(f"Tiempo total de ordenamiento: {tiempo}")

        elif opc2 == 5:
            print("\t --------------------------Binaria--------------------------")
            def binaria(array):
                for i in range(1, len(array)):
                    print(array)
                    t = array[i]
                    ordenamiento = binary_search(array, t, 0, i) + 1
                    for k in range(i, ordenamiento, -1):
                        array[k] = array[k - 1]
                    array[ordenamiento] = t
                print(array)

            def binary_search(array, k, s, end):
                if end - s <= 1:
                    if k < array[s]:
                        return s - 1
                    else:
                        return s

                media = (s + end)//2
                if array[media] < k:
                    return binary_search(array, k, media, end)
                elif array[media] > k:
                    return binary_search(array, k, s, media)
                else:
                    return media
            
            lista = [int(random.randint(0,100)) for i in range (20)]
            inicio=time.time()
            print("LISTA ALEATORIA")
            print(f"{lista} \n-------------------------------------------------------------------------------")
            binaria(lista)
            final=time.time()
            tiempo = (final-inicio)
            print(f"Tiempo total de ordenamiento: {tiempo}")

        elif opc2 == 6:
            print("\t --------------------------Quicksort--------------------------")
            def Quicksort(array):
                if len(array)<=1:
                    return array
                r=array.pop()
                p=[]
                q=[]
                for i in array:
                    if i <= r:
                        p.append(i)
                    else:
                        q.append(i)
                    print(array)
                p=Quicksort(p)
                q=Quicksort(q)
                return p + [r] + q
    
            lista = [int(random.randint(0,100)) for i in range (20)]
            inicio=time.time()
            print("LISTA ALEATORIA:")
            print(f"{lista}\n-----------------------------------------------------------")
            print(f"\nLISTA ORDENADA:\n{Quicksort(lista)}")
            final=time.time()
            iempo = (final-inicio)
            print(f"Tiempo total de ordenamiento: {tiempo}")

        elif opc2 == 7:
            print("\t --------------------Mergesort--------------------")
            def mergesort(array, array1):                                                                                   
                array2=[]
                while (len(array)> 0 and len(array2)>0):
                    if array[0]<array1[0]:
                        array2.append(array[0])
                        array=array[1:]
                    else:
                        array2.append(array1[0])
                        array1=array1[1:]
                    print(array2)

                if len(array1)>0:
                    array2=array2+array
                if len(array2)>0:
                    array2=array2+array1
                return array2
            def mer (array):                                                                                      
                if len(array)==1:
                    return array
                arrayizq=array[:len(array)//2]
                arrayder=array[len(array)//2:]

                arrayizq=mer(arrayizq)
                arrayder=mer(arrayder)

                return mergesort(arrayizq, arrayder)
            lista = [int(random.randint(0,100)) for i in range (20)]
            inicio = time.time()
            print("LISTA ALEATORIA: \n")
            print(f"{lista}\n-----------------------------------------------------------")
            print(f"\n\nLISTA ORDENADA: \n{mer(lista)}")
            final=time.time()
            tiempo = (final-inicio)
            print(f"Tiempo total de ordenamiento: {tiempo}")

    elif opc == 2:
        print("\nA que tipo de busqueda quieres ingresar?")
        print(" 1. Búsqueda secuencial")
        print(" 2. Búsqueda bicotómica")
       
        opc3 = int(input("Ingresa opcion: "))

        if opc3 == 1:
            print("\t --------------------Búsqueda secuencial--------------------")
            def secuencial(array, num):
                for i in range(0,len(array)):
                    if num==array[i]:
                        print("El numero ha sido encontrado")
                        print(f"El numero está en la posición {i} ")
                        break
                return("Numero no encontrado en la lista")
            lista = [int(random.randint(0,100)) for i in range (20)]
            inicio=time.time()
            print(f"LISTA ALEATORIA:\n {lista}\n-----------------------------------")
            nume = int(input("Ingresa un numero que quieras buscar: "))
            buscar=secuencial(lista,nume)
            final=time.time()
            tiempo = (final-inicio)
            print(f"Tiempo total de busqueda: {tiempo}")


        if opc3 == 2:
            print("\t --------------------Búsqueda bicotómica--------------------")
            def bicotomica (array,num) :
                for i , item in enumerate (array) :
                 if item == num :
                    print("El numero ha sido encontrado")
                    return f"El número {num} se encuentra en la posición {i+1} de la lista."
                return "El numero no ha sido encontrado"
            lista = [int(random.randint(0,100)) for i in range (20)]
            inicio=time.time()
            print(f"LISTA ALEATORIA:\n{lista}\n-----------------------------------------------")       
            nume = int(input("Ingresa un numero que quieras buscar: "))
            buscar = bicotomica(lista,nume)
            print(buscar)
            final=time.time()
            tiempo= (final-inicio)
            print(f"Tiempo total de busqueda: {tiempo}")





    elif opc == 3:
        break 
       
    else:
        print("     Opción no valida:(") 
    
 
